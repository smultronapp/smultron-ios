//
//  DetailViewController.swift
//  smultron
//
//  Created by Anders Planting on 2020-04-04.
//  Copyright © 2020 Anders Planting. All rights reserved.
//

import UIKit
import MapKit

class ResultViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var annotationBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var annotationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var annotationDetailButton: UIButton!
    
    func configureView() {
        // Update the user interface for the detail item.
        if let searchText = searchText {
            if let searchBar = searchBar {
                
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestWhenInUseAuthorization()
                locationManager.requestLocation()
                
                if !searchText.isEmpty {
                    searchBar.text = searchText
                    performLocationSearch(searchString: searchText)
                }
            }
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var searchText: String? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    func showNoResultAlert() {
        let alert = UIAlertController(title: "No results", message: "Sorry, we couldn't find any places", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
    }
    
    func performLocationSearch(searchString: String) {
        
        // Clear annotations
        mapView.removeAnnotations(mapView.annotations)
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBar.text
        request.region = mapView.region

        let search = MKLocalSearch(request: request)
        
        search.start { response, error in
            guard let response = response else {
                self.showNoResultAlert()
                return
            }

            for item in response.mapItems {
                if (item.timeZone == TimeZone.current) {
                    self.mapView.addAnnotation(item.placemark)
                }
            }
            
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)

        }
    }
    
    func animateBottomAnnotation(isShown: Bool) {
        
        if isShown {
            annotationBottomConstraint.constant = 0
        } else {
            annotationBottomConstraint.constant = -annotationHeightConstraint.constant
        }
        
        self.view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ResultViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print("error:: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            self.mapView.setRegion(region, animated: false)
        }
    }
}

extension ResultViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        annotationDetailButton.setTitle(view.annotation?.description, for: UIControl.State.normal)
        mapView.setCenter(view.annotation!.coordinate, animated: true)
        animateBottomAnnotation(isShown: true);
    }
    
   func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        animateBottomAnnotation(isShown: false);
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
    }
}

extension ResultViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        performLocationSearch(searchString: searchBar.text!)
    }

}


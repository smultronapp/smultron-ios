//
//  MasterViewController.swift
//  smultron
//
//  Created by Anders Planting on 2020-04-04.
//  Copyright © 2020 Anders Planting. All rights reserved.
//

import UIKit
import CoreData

class StartViewController: UIViewController {

    var resultViewController: ResultViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    
    @IBOutlet weak var searchTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let split = splitViewController {
            let controllers = split.viewControllers
            resultViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? ResultViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showResult" {
            let controller = (segue.destination as! UINavigationController).topViewController as! ResultViewController;
            controller.searchText = searchTextField.text
        }
    }
}

